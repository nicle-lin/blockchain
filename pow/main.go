package main

import (
	"github.com/nicle-lin/go-learning/blockchain/pow/blc"
	"fmt"
)

func main() {
	fmt.Println("--------------开始挖矿-------------")
	//创建创世区块
	blockChain := blc.CreateBlockChainWithGenesisBlock()

	//创建新的区块
	blockChain.AddBlockChain("Send $100 to Bruce", blockChain.Blocks[len(blockChain.Blocks)-1].Height+1, blockChain.Blocks[len(blockChain.Blocks)-1].Hash)
	blockChain.AddBlockChain("Send $200 to Apple", blockChain.Blocks[len(blockChain.Blocks)-1].Height+1, blockChain.Blocks[len(blockChain.Blocks)-1].Hash)
	blockChain.AddBlockChain("Send $300 to Alice", blockChain.Blocks[len(blockChain.Blocks)-1].Height+1, blockChain.Blocks[len(blockChain.Blocks)-1].Hash)
	blockChain.AddBlockChain("Send $400 to Bob", blockChain.Blocks[len(blockChain.Blocks)-1].Height+1, blockChain.Blocks[len(blockChain.Blocks)-1].Hash)

	fmt.Printf("创建的区块链为:\t%v\n", blockChain)
	fmt.Printf("区块链存储的区块为:\t%v\n", blockChain.Blocks)
	fmt.Printf("第二个区块的数据信息(交易信息)为:\t%v\n", string(blockChain.Blocks[1].Data))
	fmt.Printf("第二个区块的随机数为:\t%v\n",blockChain.Blocks[1].Nonce)

	//检查第二个挖出来的区块是否合法有效
	pow := blc.NewProofOfWork(blockChain.Blocks[1])
	fmt.Printf("每二个区块是否有效:%v\n",pow.IsValid())

}
