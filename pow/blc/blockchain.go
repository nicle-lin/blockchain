package blc

type BlockChain struct {
	Blocks []*Block  //存储有序的区块
}

func (bc *BlockChain)AddBlockChain(data string,height int64,preHash []byte){
	//创建新区块
	newBlock := NewBlock(data,height,preHash)
	//往链中添加区块
	bc.Blocks = append(bc.Blocks,newBlock)
}


//创建带有创世区块的区块链
func CreateBlockChainWithGenesisBlock()*BlockChain{
	genesisBlock := CreateGenesisBlock("Genesis Data..")
	return &BlockChain{[]*Block{genesisBlock}}
}


