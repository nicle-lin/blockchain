package blc

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"math/big"
	"time"
)

type ProofOfWork struct {
	Block  *Block   //当前要验证的区块
	target *big.Int //大数存储,区块难度
}

//256位Hash里面至少要有16个零0000 0000 0000 0000
const targetBit = 16

func (pow *ProofOfWork) prePareData(nonce int) []byte {
	return bytes.Join(
		[][]byte{
			pow.Block.PreBlockHash,
			pow.Block.Data,
			Int64ToBytes(pow.Block.TimeStamp),
			Int64ToBytes(int64(targetBit)),
			Int64ToBytes(int64(nonce)),
			Int64ToBytes(int64(pow.Block.Height)),
		},
		[]byte{},
	)
}

func (pow *ProofOfWork) Run(num int64) ([]byte, int64) {
	//判断Hash的有效性,如果满足条件循环休
	nonce := 0
	var hashInt big.Int //存储新生成的hash值
	var hash [32]byte

	for {
		//将Block的属性拼接成字节数组
		dataBytes := pow.prePareData(nonce)

		//生成Hash
		hash = sha256.Sum256(dataBytes)
		fmt.Printf("第%v次挖矿中...%x\n",nonce, hash)
		//将hash存储到hashInt
		hashInt.SetBytes(hash[:])
		//4.判断hashInt是否小于Block里面的target
		// Cmp compares x and y and returns:
		//
		//   -1 if x <  y
		//    0 if x == y
		//   +1 if x >  y
		//需要hashInt(y)小于设置的target(x)
		if pow.target.Cmp(&hashInt) == 1 {
			fmt.Printf("时间为:%v,第%v个区块挖矿成功:%x\n", time.Now(), num, hash)
			time.Sleep(5 * time.Second)
			break
		}
		nonce++
	}
	return hash[:], int64(nonce)
}

//创建新的工作量证明对象
func NewProofOfWork(block *Block) *ProofOfWork {
	/*1.创建初始值为1的target
		0000 0001
		8 - 2
	*/
	target := big.NewInt(1)

	//左移256-targetBit
	target = target.Lsh(target,256-targetBit)
	return &ProofOfWork{Block:block,target:target}
}

//判断挖矿得到区块是否有效
func (pow *ProofOfWork)IsValid()bool{
	var hashInt big.Int
	hashInt.SetBytes(pow.Block.Hash)
	if pow.target.Cmp(&hashInt) == 1{
		return true
	}
	return false
}
