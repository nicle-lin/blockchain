package blc

import (
	"strconv"
	"bytes"
	"crypto/sha256"
	"time"
)

//定义区块
type Block struct {
	//区块高度,也就是区块的编号,第几个区块
	Height int64
	//上一个区块的hash值
	PreBlockHash []byte
	//交易数据(最终都属于transaction事务)
	Data []byte
	//创建时间的时间戳
	TimeStamp int64
	//当前区块的hash值
	Hash []byte
	//随机数,用于验证工作量证明
	Nonce int64
}

//定义区块生成Hash的方法
func (block *Block)SetHash(){
	//将Height转换为字节数组[]byte
	heightBytes := Int64ToBytes(block.Height)
	//将TimeStamp 转换为二进制字节数组[]byte
	timeBytes := []byte(strconv.FormatInt(block.TimeStamp,2))
	//拼接所有属性,形成一个二维的byte数组
	blockBytes := bytes.Join(
		[][]byte{
			heightBytes,
			block.PreBlockHash,
			block.Data,
			timeBytes,
			block.Hash,
		},
		[]byte{},
	)
	hash := sha256.Sum256(blockBytes)
	block.Hash = hash[:]
}

// 创建新的区块
func NewBlock(data string, height int64, preBlockHash []byte)*Block{
	block := &Block{
		Height:height,
		PreBlockHash:preBlockHash,
		Data:[]byte(data),
		TimeStamp:time.Now().Unix(),
		Nonce:0,
	}
	//调用工作量证明的方法，并且返回有效的Hash和Nonce值
	//创建pow对象
	pow := NewProofOfWork(block)
	hash, nonce := pow.Run(height)
	block.Hash = hash[:]
	block.Nonce = nonce
	return block
}

//生成创世区块
func CreateGenesisBlock(data string) *Block{
	return NewBlock(data,1,
		[]byte{
			0, 0, 0, 0, 0,
			0, 0, 0, 0, 0,
			0, 0, 0, 0, 0,
			0, 0, 0, 0, 0,
			0, 0, 0, 0, 0,
			0, 0, 0, 0, 0,
			0, 0})
}