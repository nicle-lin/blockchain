package blc

import (
	"bytes"
	"encoding/binary"
)

func Int64ToBytes(v int64)[]byte{
	buf := bytes.NewBuffer([]byte{})
	binary.Write(buf,binary.BigEndian,v)
	return buf.Bytes()
}

func BytesToInt64(b []byte)int64{
	buf := bytes.NewBuffer(b)
	var v int64
	binary.Read(buf,binary.BigEndian,&v)
	return v
}
